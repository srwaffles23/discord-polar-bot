import discord
from discord.ext import commands
import random
import parser
import re

description = '''An example bot to showcase the discord.ext.commands extension
module.
There are a number of utility commands being showcased here.'''

token = 'MTY4MTc1Nzg0NTQ3MDU3NjY0.XdcLDw.l-dwjNaNBeeolqTbxsgpkLEwQJA'

class MyClient(discord.Client):
    async def on_ready(self):
        print('Connected!')
        print('Username: {0.name}\nID: {0.id}'.format(self.user))

    async def on_message(self, message):
        if message.content.startswith('polar'):
            #ejecutar(analiza (message))
            #print(resutl)
            palabras = re.findall("[\w']+", message.content.lower())
            print(palabras)
            middle = analyze(palabras)
            try:
                msg = await message.channel.send(sugest(middle))
            except:
                msg = await message.channel.send(middle)


    async def on_message_delete(self, message):
        fmt = '{0.author} has deleted the message: {0.content}'
        await message.channel.send(fmt.format(message))


def sugest(lexemas):
    if lexemas[-1].tag == 'ADJ':
        if lexemas[-1].pos == 'feliz':
            lines = open('happy.txt').read().splitlines()
            return ('Vivela pues, para que estes mejor: ' + random.choice(lines))
        if lexemas[-1].pos == 'triste':
            lines = open('sad.txt').read().splitlines()
            return ('Como asi mostro? Relajao, aqui tengo algo pa ti: ' + random.choice(lines))
        if lexemas[-1].pos == 'molesto':
            lines = open('mad.txt').read().splitlines()
            return ('Bajale dos. Escucha esta vaina: ' + random.choice(lines))
        if lexemas[-1].pos == 'enamorado':
            lines = open('love.txt').read().splitlines()
            return ('Asi es la vaina? Dedicale esta pue: ' + random.choice(lines))
        if lexemas[-1].pos == 'aburrido':
            lines = open('bored.txt').read().splitlines()
            return ('Cambia ese mood aburrido por un mood chill con esta rolita: ' + random.choice(lines))
    else:
        print ('[!] - Error Sintactico - Esta oracion no es valida')
        return "Error Sintactico - Esta oracion no es valida"



def analyze(palabras):
    tokens = []
    for palabra in palabras:
        if palabra == 'polar':
            continue
        x = parser.Lexema(palabra)
        print(x.pos)
        x.test()
        if x.valid:
            tokens.append(x)
        else:
            print ('[!] - Error Lexico - Este token no es valido con la gramatica actual')
            return "Error Lexico - Este token no es valido con la gramatica actual"
    return tokens



client = MyClient()
client.run(token)
