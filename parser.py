import re
import sys

#Gramatica del Lenguaje
suj = ['me', 'yo']
verb = ['siento', 'estoy']
adj = ['feliz', 'triste', 'aburrido', 'molesto', 'enamorado']
valid_tokens = ['SUJ', 'VERB', 'ADJ']
pred = ['VERBADJ']
valid_sent = ['PRED', 'SUJPRED', 'SUJVERBADJ', 'VERBADJ']


#text = sys.argv[1]

class Lexema:
    def __init__(self, pos):
        self.pos = pos
        self.tag = self.taggin(pos)
        self.valid = self.isvalid()
        
    def taggin(self, token):
        for tk in suj:
            if token == tk:
                return 'SUJ'
        for tk in verb:
            if token == tk:
                return 'VERB'
        for tk in adj:
            if token == tk:
                return 'ADJ'

    def test(self):
        if self.valid:
            print("true")
        else:
            print("false")

    def isvalid(self):
        for tk in valid_tokens:
            # Error interno
            try:
                print("DEBUG: "+ self.tag + "==" + tk)
            except:
                print("TOKEN INVALIDO: " + self.pos)
                break
            
            #Atributo self.valid a false
            if self.tag != tk:
                continue
            else:
                return True


#text = sys.argv[1]
#Tokenizar
#palabras = re.findall("[\w']+", text.lower())
#print (palabras)
#for palabra in palabras:
#    print("DEBUG: " + palabra)
#    x = Lexema(palabra)
#    tokens.append(x)


#debug
#for obj in tokens:
#    print (obj.pos + "+" + obj.tag)


#Validar Sintaxis
#for test in valid_sent:
#    if (tokens[0].tag+tokens[1].tag+tokens[2].tag) == test:
#        print ("Valid")
